class Board

  attr_accessor :grid

  def initialize(grid=[[nil, nil, nil], [nil, nil, nil],[nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

  def winner
    checks = [check_row, check_col, check_right_diag, check_left_diag]
    checks.reject { |check| check.nil? }.first
  end

  def over?
    if !winner.nil?
      return true
    elsif !@grid.flatten.include?(nil)
      return true
    end
    false
  end

  private

  def check_row
    @grid.each do |row|
      if !row[0].nil?
        return row[0] if row[0] == row[1] && row[0] == row[2]
      end
    end
    nil
  end

  def check_col
    @grid[0].each_with_index do |col, idx|
      if !col.nil?
        return col if col == @grid[1][idx] && col == @grid[2][idx]
      end
    end
    nil
  end

  def check_right_diag
    check = @grid[0][0]
    if !check.nil?
      return check if check == @grid[1][1] && check == @grid[2][2]
    end
    nil
  end

  def check_left_diag
    check = @grid[0][2]
    if !check.nil?
      return check if check == @grid[1][1] && check == @grid[2][0]
    end
    nil
  end

end
