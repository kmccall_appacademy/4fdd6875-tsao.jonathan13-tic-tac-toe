require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def board
    return @board
  end

  def current_player
    @current_player
  end

  def switch_players!
    case @current_player
    when @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play_turn
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    switch_players!
  end

end
