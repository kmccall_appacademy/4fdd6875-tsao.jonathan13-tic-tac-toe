class ComputerPlayer

  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = potential_moves
    moves.each_with_index do |move, idx|
      temp = @board.dup
      temp.place_mark(move, @mark)
      return move if !temp.winner.nil?
    end
    moves[rand(0...moves.length)]
  end

  private

  def potential_moves
    result = []
    @board.grid.each_with_index do |row, idx1|
      row.each_with_index do |el, idx2|
        result << [idx2, idx1] if @board.empty?([idx1, idx2])
      end
    end
    result
  end

end
